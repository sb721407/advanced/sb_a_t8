resource "yandex_compute_instance" "proxy" {
  name        = "proxy"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8crmu3ho2ciir7b26c" # lemp
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "app1" {
  name        = "app1"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8crmu3ho2ciir7b26c" # lemp
      #      image_id = "fd8ehokd8vmtrada0skq" # lamp
      size = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-2.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-app1.id]
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "app2" {
  name        = "app2"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8crmu3ho2ciir7b26c" # lemp
      #image_id = "fd8ehokd8vmtrada0skq" # lamp
      size = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-3.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-app2.id]
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "db1" {
  name        = "db1"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd8crmu3ho2ciir7b26c" # lemp
      image_id = "fd8ehokd8vmtrada0skq" # lamp
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-4.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-db1.id]
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "db2" {
  name        = "db2"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd8crmu3ho2ciir7b26c" # lemp
      image_id = "fd8ehokd8vmtrada0skq" # lamp
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-5.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-db2.id]
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### DNS A-records #####

resource "yandex_dns_recordset" "proxy_dns_name" {
  depends_on = [yandex_compute_instance.proxy]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "proxy"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.proxy.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "app1_dns_name" {
  depends_on = [yandex_compute_instance.app1]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "app1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.app1.network_interface.0.ip_address]
}

resource "yandex_dns_recordset" "app2_dns_name" {
  depends_on = [yandex_compute_instance.app2]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "app2"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.app2.network_interface.0.ip_address]
}

resource "yandex_dns_recordset" "db1_dns_name" {
  depends_on = [yandex_compute_instance.db1]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "db1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.db1.network_interface.0.ip_address]
}

resource "yandex_dns_recordset" "db2_dns_name" {
  depends_on = [yandex_compute_instance.db2]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "db2"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.db2.network_interface.0.ip_address]
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[proxy]
${yandex_compute_instance.proxy.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[app1]
${yandex_compute_instance.app1.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[app2]
${yandex_compute_instance.app2.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[db1]
${yandex_compute_instance.db1.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[db2]
${yandex_compute_instance.db2.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[all]
${yandex_compute_instance.proxy.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'
${yandex_compute_instance.app1.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'
${yandex_compute_instance.app2.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'
${yandex_compute_instance.db1.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'
${yandex_compute_instance.db2.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'
EOF
  filename = "${path.module}/inventory"
}

##### Create file proxy.conf #####

resource "local_file" "proxy_conf" {
  content  = <<EOF
upstream backend {
  server ${yandex_compute_instance.app1.network_interface.0.ip_address}; 
  server ${yandex_compute_instance.app2.network_interface.0.ip_address};
  }
  server {
    listen 80; 
    location / {
    proxy_pass http://backend;
  }
}
EOF
  filename = "${path.module}/proxy.conf"
}

##### Create file app1.conf #####

resource "local_file" "app1_conf" {
  content  = <<EOF
upstream backend {
  server ${yandex_compute_instance.db1.network_interface.0.ip_address};
  }
  server {
    listen 80;
    location / {
    proxy_pass http://backend;
  }
}
EOF
  filename = "${path.module}/app1.conf"
}

##### Create file app2.conf #####

resource "local_file" "app2_conf" {
  content  = <<EOF
upstream backend {
  server ${yandex_compute_instance.db2.network_interface.0.ip_address};
  }
  server {
    listen 80;
    location / {
    proxy_pass http://backend;
  }
}
EOF
  filename = "${path.module}/app2.conf"
}

##### Create file index.html for db1 #####

resource "local_file" "db1_conf" {
  content  = <<EOF
<!doctype html>
<html lang="en">
    <head>
        <title>Your page's title goes here - DB1</title>
    </head>
    <body>
        <h1>This is a heading - DB1</h1>
        <p>And this is a paragraph - DB1</p>
    </body>
</html>
EOF
  filename = "${path.module}/index1.html"
}

##### Create file index.html for db2 #####

resource "local_file" "db2_conf" {
  content  = <<EOF
<!doctype html>
<html lang="en">
    <head>
        <title>Your page's title goes here - DB2</title>
    </head>
    <body>
        <h1>This is a heading - DB2</h1>
        <p>And this is a paragraph - DB2</p>
    </body>
</html>
EOF
  filename = "${path.module}/index2.html"
}

##### Provisioning #####

resource "null_resource" "proxy" {
  depends_on = [yandex_compute_instance.proxy, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.proxy.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ~/.ssh/id_rsa ../ansible/proxy.yml"
  }
}

resource "null_resource" "app1" {
  depends_on = [yandex_compute_instance.app1, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.app1.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ~/.ssh/id_rsa ../ansible/app1.yml"
  }
}

resource "null_resource" "app2" {
  depends_on = [yandex_compute_instance.app2, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.app2.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ~/.ssh/id_rsa ../ansible/app2.yml"
  }
}

resource "null_resource" "db1" {
  depends_on = [yandex_compute_instance.db1, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.db1.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ~/.ssh/id_rsa ../ansible/db1.yml"
  }
}

resource "null_resource" "db2" {
  depends_on = [yandex_compute_instance.db2, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.db2.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ~/.ssh/id_rsa ../ansible/db2.yml"
  }
}

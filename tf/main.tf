##### Variables #####

locals {
  local_data = jsondecode(file("${path.module}/config.json"))
}

##### General #####

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token = file(local.local_data.OAUTH_FILE)
  #  service_account_key_file = local.local_data.KEY_FILE
  cloud_id  = local.local_data.CLOUD_ID
  folder_id = local.local_data.FOLDER_ID
  zone      = local.local_data.ZONE
}

##### Net & Subnet definition #####
##### Network1 Subnets 1-5 #####

resource "yandex_vpc_network" "network-1" {
  name = "network-1"
}
resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet-1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}
resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet-2"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.2.0/24"]
}
resource "yandex_vpc_subnet" "subnet-3" {
  name           = "subnet-3"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.3.0/24"]
}
resource "yandex_vpc_subnet" "subnet-4" {
  name           = "subnet-4"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.4.0/24"]
}
resource "yandex_vpc_subnet" "subnet-5" {
  name           = "subnet-5"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.5.0/24"]
}
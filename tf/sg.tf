resource "yandex_vpc_security_group" "sg-app1" {
  name        = "sg-app1"
  description = "Security Group for APP1"
  network_id  = yandex_vpc_network.network-1.id
  ingress {
    protocol       = "TCP"
    description    = "Web Service from Proxy"
    v4_cidr_blocks = ["192.168.1.0/24", "192.168.2.0/24", "192.168.3.0/24", "192.168.4.0/24"]
    port           = 80
  }
  ingress {
    protocol       = "TCP"
    description    = "SSH Service to Proxy"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "yandex_vpc_security_group" "sg-app2" {
  name        = "sg-app2"
  description = "Security Group for APP2"
  network_id  = yandex_vpc_network.network-1.id
  ingress {
    protocol       = "TCP"
    description    = "Web Service from Proxy"
    v4_cidr_blocks = ["192.168.1.0/24", "192.168.2.0/24", "192.168.3.0/24", "192.168.5.0/24"]
    port           = 80
  }
  ingress {
    protocol       = "TCP"
    description    = "SSH Service to Proxy"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "yandex_vpc_security_group" "sg-db1" {
  name        = "sg-db1"
  description = "Security Group for db1"
  network_id  = yandex_vpc_network.network-1.id
  ingress {
    protocol       = "TCP"
    description    = "Web Service"
    v4_cidr_blocks = ["192.168.2.0/24"]
    port           = 80
  }
  ingress {
    protocol       = "TCP"
    description    = "SSH Service to Proxy"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
#  egress {
#    protocol       = "ANY"
#    description    = "any"
#    v4_cidr_blocks = ["0.0.0.0/0"]
#  }
  egress {
    protocol       = "TCP"
    description    = "Web Service"
    v4_cidr_blocks = ["192.168.2.0/24"]
    port           = 80
  }
  egress {
    protocol       = "TCP"
    description    = "SSH Service to Proxy"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
}

resource "yandex_vpc_security_group" "sg-db2" {
  name        = "sg-db2"
  description = "Security Group for DB2"
  network_id  = yandex_vpc_network.network-1.id
  ingress {
    protocol       = "TCP"
    description    = "Web Service"
    v4_cidr_blocks = ["192.168.3.0/24"]
    port           = 80
  }
  ingress {
    protocol       = "TCP"
    description    = "SSH Service to Proxy"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
  egress {
    protocol       = "TCP"
    description    = "Web Service"
    v4_cidr_blocks = ["192.168.3.0/24"]
    port           = 80
  }
  egress {
    protocol       = "TCP"
    description    = "SSH Service to Proxy"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
}
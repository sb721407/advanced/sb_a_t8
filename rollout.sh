#!/bin/bash

function stop_app() {
    cd tf
    terraform init
    terraform validate
    terraform destroy -auto-approve
}
function start_app() {
    cd tf
    terraform init
    terraform validate
    terraform apply -auto-approve
}
case "$1" in 
    stop)   stop_app ;;
    start)    start_app ;;
    *) echo "usage: sh $0 start|stop" >&2
       exit 1
       ;;
esac